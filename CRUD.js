grupo={
  "clave":"g11111",
  "nombre":"ISA",
  "docente":["d11111","d22222","d33333","d44444","d55555"],
  "materia":["m11111","m22222","m33333","m44444","m55555"],
  "alumno":["a11111","a22222","a33333","a44444","a55555"]
}
docente={
  "clave":["d11111","d22222","d33333","d44444","d55555"],
  "nombre":["ANTONIO","LORENZO ALEJANDRO","IDARH CLAUDIO","JOHANN FRANCISCO","ANGEL AUGUSTO"],
  "apellido":["HERNÁNDEZ BLAS","MATADAMAS TORRES","MATADAMAS ORTIZ","JIMENEZ HALLA","BRAVO SUMANO"],
  "g_academico":["INGENIERO","MAESTRIA","MAESTRIA","ING","ING"]
}
alumno={
  "clave" :["a11111","a22222","a33333","a44444","a55555"],
  "nombre":["Luis Ricardo","Julio Cesar","Nancy Emma","Luz Adriana","Juan Carlos"],
  "apellido":["Cayetano Herrera","Hernandez Gomez","Ramirez Tejada","Bautista Joaquin","Garcia Avendaño"],
  "calificacion" :["90","70","80","85","100"]
}
materia={
  "clave":["m11111","m22222","m33333","m444444","m55555"],
  "nombre":["Herramientas de Programacion Web","Gestion de Proyectos de Software","Inteligencia Aritficial","Administracion de Redes","Sistemas Programables"]
}

//////////////////////////////DOCENTES//////////////////////////////////////

//Operacion "Read" lista de docentes de un grupo 
var readDocenteGrupo = function(grupo){
  console.log("Clave\t|\t Nombre");
  for(var i=0;i<grupo["docente"].length;i++){
    for(var j=0;j<docente["clave"].length;j++){
      if(grupo["docente"][i]==docente["clave"][j]){
        console.log(docente["clave"][j]+"\t\t",docente["nombre"][j]);
        console.log("\n");
      }
      
    }
  }
}
readDocenteGrupo(grupo);

//Operacion "Create" docente
var createDocente= function(docente,c,n,a,g){
  var mensaje;
  var estado;
  for(var i=0;i<docente["clave"].length;i++){
    if(docente["clave"][i]==c){
      estado=true;
    }
    else 
    {
      estado=false;
    }
  }
  
  if(estado === false){
    var posicion = (docente["clave"].length);
    docente["clave"][posicion]=c;
    docente["nombre"][posicion]=n;
    docente["apellido"][posicion]=a;
    docente["g_academico"][posicion]=g;
    
    mensaje="Docente agredo exitosamente";  
  }
  else{
    mensaje="El doncente ya existe, intente de nuevo!";     
  }
  
  console.log(mensaje);   
}
createDocente(docente,"d66666","nombre1","apellido1","ING");

//Operacion "Read" lista todos los Docentes
var  readDocente = function(docente){
  console.log("| Clave\t|\t Nombre\t\t|\t Apellido\t|");
    for(var j=0;j<docente["clave"].length;j++){
        console.log(docente["clave"][j]+"\t\t",docente["nombre"][j]+"\t\t",docente["apellido"][j]+"\t\t");
        console.log("\n");
    }
}
readDocente(docente);

//Consulta de un docente en un grupo por su clave
var consultaDocenteGrupo= function (grupo,claveX)
{
  for(var i=0;i<grupo["alumno"].length;i++){
    for(var j=0;j<docente["clave"].length;j++){
      if(grupo["docente"][i]==docente["clave"][j]){
        if(docente["clave"][j] == claveX )
        {
          console.log(docente["clave"][j]+"\t\t",docente["nombre"][j]);
          console.log("\n");
        }
      }
      
    }
  }
}
consultaDocenteGrupo(grupo,"d11111");

//Buscar Docente (para la operacion "Update" y "Delete")
var buscarDocente = function(docente,c){
  for(var i = 0; i<docente["clave"].length; i++){
    if(docente["clave"][i]==c){
      return i;
    }
  }
  return -1;
}
buscarDocente(docente,"d11111");

//Consulta de docente de la lista de docentes por su clave
var consultaDocente= function (docente,c)
{
  var x = buscarDocente(docente,c);
  var mensaje;
  if(x!=-1){
    mensaje=(docente["clave"][x]+"\t\t",docente["nombre"][x]+"\n");
  }
  else{
    mensaje="El Docente no existe, intente de nuevo";
  }
  console.log(mensaje);
}
consultaDocente(docente,"d11111");


//Operacion "Update" Docente
var updateDocente= function (docente,c,n,a,g){
  var x = buscarDocente(docente,c);
  var mensaje;
  if(x!=-1){
    docente["nombre"][x]= n;
    docente["apellido"][x]= a;
    docente["g_academico"][x]= g;
    
    mensaje="El Docente fue modificado exitosamemte";
  }else{
    mensaje="El doncente no existe, intente de nuevo!";
  }
  console.log(mensaje)
}
updateDocente(docente,"d66666","nombre2","apellido2","ING2");

//Operacion "Delete" Docente
var deleteDocente = function(docente, c){
  var x = buscarDocente(docente,c);
  var mensaje;
  if(x!=-1){
    docente["clave"][x]= undefined;
    docente["nombre"][x]= undefined;
    docente["apellido"][x]= undefined;
    docente["g_academico"][x]= undefined;
    x > -1 && docente["clave"].splice( x, 1 );
    x > -1 && docente["nombre"].splice( x, 1 );
    x > -1 && docente["apellido"].splice( x, 1 );
    x > -1 && docente["g_academico"].splice( x, 1 );
    
    mensaje="El Docente fue eliminado exitosamemte";
  }else{
    mensaje("El Docente introducido no existe");
  }
  console.log(mensaje);
}
deleteDocente(docente,"d66666");


/////////////////////////ALUMNOS/////////////////////////////////////

//Operacion "Read" lista de alumnos de un grupo 
var  readAlumnoGrupo = function(grupo){
  console.log("Clave\t|\t Nombre");
  for(var i=0;i<grupo["alumno"].length;i++){
    for(var j=0;j<alumno["clave"].length;j++){
      if(grupo["alumno"][i]==alumno["clave"][j]){
        console.log(alumno["clave"][j]+"\t\t",alumno["nombre"][j]);
        console.log("\n");
      }
      
    }
  }
}
readAlumnoGrupo(grupo);

//Operacion "Create" alumno
var createAlumno= function(alumno,c,n,a,ca){
  var mensaje;
  var estado;
  for(var i=0;i<alumno["clave"].length;i++){
    if(alumno["clave"][i]==c){
      estado=true;
    }
    else 
    {
      estado=false;
    }
  }
  
  if(estado === false){
    var posicion = (alumno["clave"].length);
    alumno["clave"][posicion]=c;
    alumno["nombre"][posicion]=n;
    alumno["apellido"][posicion]=a;
    alumno["calificacion"][posicion]=ca;
    
    mensaje="Alumno agredo exitosamente";  
  }
  else{
    mensaje="El Alumno ya existe, intente de nuevo!";     
  }
  
  console.log(mensaje);   
}
createAlumno(alumno,"a66666","nombre1","apellido1","100");

//Operacion "Read" lista todos los Alumnos
var  readAlumno = function(alumno){
  console.log("| Clave\t|\t Nombre\t\t|\t Apellido\t|");
    for(var j=0;j<alumno["clave"].length;j++){
        console.log(alumno["clave"][j]+"\t\t",alumno["nombre"][j]+"\t\t",alumno["apellido"][j]+"\t\t");
        console.log("\n");
    }
}
readAlumno(alumno);

//Consulta de un alumno en un grupo por su clave
var consultaAlumnoGrupo= function (grupo,claveX)
{
  for(var i=0;i<grupo["alumno"].length;i++){
    for(var j=0;j<alumno["clave"].length;j++){
      if(grupo["alumno"][i]==alumno["clave"][j]){
        if(alumno["clave"][j] == claveX )
        {
          console.log(alumno["clave"][j]+"\t\t",alumno["nombre"][j]);
          console.log("\n");
        }
      }
      
    }
  }
}
consultaAlumnoGrupo(grupo,"a11111");

//Buscar alumno (para la operacion "Update" y "Delete")
var buscarAlumno = function(alumno,c){
  for(var i = 0; i<alumno["clave"].length; i++){
    if(alumno["clave"][i]==c){
      return i;
    }
  }
  return -1;
}
buscarAlumno(alumno,"a11111");

//Consulta de alumno de la lista de Alumnos por su clave
var consultaAlumno= function (alumno,c)
{
  var x = buscarAlumno(alumno,c);
  var mensaje;
  if(x!=-1){
    mensaje=(alumno["apellido"][x]+"\t\t",alumno["nombre"][x]+"\n");
  }
  else{
    mensaje="El alumno no existe, intente de nuevo";
  }
  console.log(mensaje);
}
consultaAlumno(alumno,"a11111");


//Operacion "Update" alumno
var updateAlumno= function (alumno,c,n,a,ca){
  var x = buscarAlumno(alumno,c);
  var mensaje;
  if(x!=-1){
    alumno["nombre"][x]= n;
    alumno["apellido"][x]= a;
    alumno["calificacion"][x]= ca;
    
    mensaje="El alumno fue modificado exitosamemte";
  }else{
    mensaje="El doncente no existe, intente de nuevo!";
  }
  console.log(mensaje)
}
updateAlumno(alumno,"a66666","nombre2","apellido2","70");

//Operacion "Delete" alumno
var deleteAlumno= function(alumno, c){
  var x = buscarAlumno(alumno,c);
  var mensaje;
  if(x!=-1){
    alumno["clave"][x]= undefined;
    alumno["nombre"][x]= undefined;
    alumno["apellido"][x]= undefined;
    alumno["calificacion"][x]= undefined;
    x > -1 && alumno["clave"].splice( x, 1 );
    x > -1 && alumno["nombre"].splice( x, 1 );
    x > -1 && alumno["apellido"].splice( x, 1 );
    x > -1 && alumno["calificacion"].splice( x, 1 );
    
    mensaje="El alumno fue eliminado exitosamemte";
  }else{
    mensaje("El alumno introducido no existe");
  }
  console.log(mensaje);
}
deleteAlumno(alumno,"a66666");



//////////////////////////////////////////////////////////////////////////7

//Operacion "Read" lista de Materias de un grupo 
var readMateriaGrupo = function(grupo){
  console.log("Clave\t|\t Nombre");
  for(var i=0;i<grupo["materia"].length;i++){
    for(var j=0;j<materia["clave"].length;j++){
      if(grupo["materia"][i]==materia["clave"][j]){
        console.log(materia["clave"][j]+"\t\t",materia["nombre"][j]);
        console.log("\n");
      }
      
    }
  }
}
readMateriaGrupo(grupo);

//Operacion "Create" materia
var createMateria= function(materia,c,n){
  var mensaje;
  var estado;
  for(var i=0;i<materia["clave"].length;i++){
    if(materia["clave"][i]==c){
      estado=true;
    }
    else 
    {
      estado=false;
    }
  }
  
  if(estado === false){
    var posicion = (materia["clave"].length);
    materia["clave"][posicion]=c;
    materia["nombre"][posicion]=n;
    
    mensaje="materia agreda exitosamente";  
  }
  else{
    mensaje="El doncente ya existe, intente de nuevo!";     
  }
  
  console.log(mensaje);   
}
createMateria(materia,"m66666","nombre1");

//Operacion "Read" lista todos los Materias
var  readMateria = function(materia){
  console.log("| Clave\t|\t Nombre\t\t\t\t|\t");
    for(var j=0;j<materia["clave"].length;j++){
        console.log(materia["clave"][j]+"\t\t",materia["nombre"][j]+"\t\t");
        console.log("\n");
    }
}
readMateria(materia);

//Consulta de un materia en un grupo por su clave
var consultaMateriaGrupo= function (grupo,claveX)
{
  for(var i=0;i<grupo["alumno"].length;i++){
    for(var j=0;j<materia["clave"].length;j++){
      if(grupo["materia"][i]==materia["clave"][j]){
        if(materia["clave"][j] == claveX )
        {
          console.log(materia["clave"][j]+"\t\t",materia["nombre"][j]);
          console.log("\n");
        }
      }
      
    }
  }
}
consultaMateriaGrupo(grupo,"m11111");

//Buscar materia (para la operacion "Update" y "Delete")
var buscarMateria = function(materia,c){
  for(var i = 0; i<materia["clave"].length; i++){
    if(materia["clave"][i]==c){
      return i;
    }
  }
  return -1;
}
buscarMateria(materia,"m11111");

//Consulta de materia de la lista de Materias por su clave
var consultaMateria= function (materia,c)
{
  var x = buscarMateria(materia,c);
  var mensaje;
  if(x!=-1){
    mensaje=(materia["clave"][x]+"\t\t",materia["nombre"][x]+"\n");
  }
  else{
    mensaje="El materia no existe, intente de nuevo";
  }
  console.log(mensaje);
}
consultaMateria(materia,"m11111");


//Operacion "Update" materia
var updateMateria= function (materia,c,n){
  var x = buscarMateria(materia,c);
  var mensaje;
  if(x!=-1){
    materia["nombre"][x]= n;
    
    mensaje="El materia fue modificado exitosamemte";
  }else{
    mensaje="El doncente no existe, intente de nuevo!";
  }
  console.log(mensaje)
}
updateMateria(materia,"m66666","Nombre2");

//Operacion "Delete" materia
var deleteMateria = function(materia, c){
  var x = buscarMateria(materia,c);
  var mensaje;
  if(x!=-1){
    materia["clave"][x]= undefined;
    materia["nombre"][x]= undefined;
    x > -1 && materia["clave"].splice( x, 1 );
    x > -1 && materia["nombre"].splice( x, 1 );
    
    mensaje="El materia fue eliminado exitosamemte";
  }else{
    mensaje("El materia introducido no existe");
  }
  console.log(mensaje);
}
deleteMateria(materia,"m66666");
